# Build
FROM golang:1.21  as build

COPY catgpt ./src/catgpt

WORKDIR /go/src/catgpt

RUN go mod download

RUN CGO_ENABLED=0 go build -o /go/main

# Production
FROM gcr.io/distroless/static-debian12:latest-amd64 as production

COPY --from=build /go/main .

ENTRYPOINT ["./main"]