terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = "./tf_key.json"
  folder_id                = local.folder_id
  zone                     = "ru-central1-a"
}

locals {
  folder_id = "b1gi4hilf09ed57e7gk2"
}

resource "yandex_container_registry" "registry" {
  name = "registry"
}

output registry_id {
  value = "${yandex_container_registry.registry.id}"
}