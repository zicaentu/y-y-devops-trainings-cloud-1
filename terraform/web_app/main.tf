terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = "./tf_key.json"
  folder_id                = local.folder_id
  zone                     = local.zone
}

locals {
  folder_id = "b1gi4hilf09ed57e7gk2"
  zone = "ru-central1-a"
}


module iam {
  source = "./modules/iam"
  folder_id  = local.folder_id
}

module network {
  source = "./modules/network"
  depends_on = [module.iam]
  zone = local.zone
}

module ig {
  source = "./modules/ig"
  depends_on = [module.iam]
  folder_id = local.folder_id
  zone = local.zone
  catgpt_network = module.network.catgpt_network
  catgpt_subnet = module.network.catgpt_subnet
  catgpt_ig_sa = module.iam.catgpt_ig_sa
}

module lb {
  source = "./modules/lb"
  depends_on = [module.iam, module.ig]
  catgpt_web_ig = module.ig.catgpt_web_ig
}
