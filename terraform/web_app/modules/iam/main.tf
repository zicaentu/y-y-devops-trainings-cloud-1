resource "yandex_iam_service_account" "catgpt_ig_sa" {
  name        = "ig-sa"
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = "${var.folder_id}"
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.catgpt_ig_sa.id}"
}