data "yandex_compute_image" "coi" {
  family = "container-optimized-image"
}

resource "yandex_compute_instance_group" "catgpt_web_ig" {
  name                = "catgpt-web-group"
  folder_id           = "${var.folder_id}"
  service_account_id  = "${var.catgpt_ig_sa.id}"
  
  instance_template {
    platform_id = "standard-v2"
    resources {
      memory = 1
      cores  = 2
      core_fraction = 5
    }
    scheduling_policy {
      preemptible = true
    }
    boot_disk {
      initialize_params {
        type = "network-hdd"
        size = "30"
        image_id = "${data.yandex_compute_image.coi.id}"
      }
    }
    network_interface {
      network_id = "${var.catgpt_network.id}"
      subnet_ids = ["${var.catgpt_subnet.id}"]
    }
    metadata = {
      docker-compose = file("${path.module}/docker-compose.yaml")
      ssh-keys  = "ubuntu:${file("~/.ssh/devops_training.pub")}"
    }
  }


  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["${var.zone}"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "auto-group-tg"
    target_group_description = "load balancer target group"
  }
}