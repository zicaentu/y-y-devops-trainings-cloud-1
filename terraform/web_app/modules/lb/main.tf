resource "yandex_lb_network_load_balancer" "catgpt_lb" {
  name = "catgpt-lb"
  listener {
    name = "listener-web-servers"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  listener {
    name = "listener-web-servers-prom"
    port = 9090
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${var.catgpt_web_ig.load_balancer.0.target_group_id}"

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/ping"
      }
    }
  }
}