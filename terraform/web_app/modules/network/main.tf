resource "yandex_vpc_network" "catgpt_network" {
  name = "catgpt-network"
}

resource "yandex_vpc_subnet" "catgpt_subnet" {
  name           = "catgpt-subnet"
  zone           = "${var.zone}"
  network_id     = "${yandex_vpc_network.catgpt_network.id}"
  v4_cidr_blocks = ["10.5.0.0/24"]
}