output catgpt_network {
  value       = "${yandex_vpc_network.catgpt_network}"
}

output catgpt_subnet {
  value       = "${yandex_vpc_subnet.catgpt_subnet}"
}
